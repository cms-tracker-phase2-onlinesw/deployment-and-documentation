# Deployment and Documentation

User guides and example deployments for the Tracker Online Software stack.

## Contents
[[_TOC_]]

## Standalone Deployment

When started out with the online software stack, it is recommended to first use the standalone version. No special hardware is required, such as the Phase II processing cards, to get started. All aspects of the online software are run within docker containers on a single computer. This allows for you to gain an understanding of how all aspects of the software work together before deploying to real hardware.

The standalone stack contains three herd-dummy containers, which provide functional examples of all the endpoints on real HERD instances. At the Shep level, the stack has all the containers required to use Shep: the API server, a web-based UI, a database to store board information and a heartbeat server.

### Prerequisites
In order to get started with the standalone stack, the [docker](https://docs.docker.com/get-docker/) container runtime environment is required to be installed on a linux computer running the x86 architecture. If you are using a CentOS8 machine, please see the [installation instructions below](#docker-on-centos8).

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the stack from the YAML file.

### Building the stack
The docker-compose YAML file for the standalone deployment can be found under [deployments/docker-compose](deployments/docker-compose). This repo should either be cloned or that directory copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Start the containers
# (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose -f docker-compose.yml -f docker-compose.devel.yml up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer). To use a port other than the default for the HTTP interface, change the port mapping for the `nginx` container within the docker-compose YAML file and start the containers again.

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md).

The stack can be stopped by running the command `docker-compose -f docker-compose.yml -f docker-compose.devel.yml down` from the directory containing the YAML file.

### Monitoring
A stack of [Loki](https://grafana.com/oss/loki/), [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) and [Grafana](https://grafana.com/oss/grafana/) is installed as part of the standalone example to serve as a monitoring solution. To be used, you need to navigate to http://localhost:3002 and login with the username `admin` and the password defined in `grafana_password.txt` (`P@ssw0rd` by default). From there you can navigate to Explore on the left to explore the logs of the system.

## HERD On Hardware + Shep Stack

The method for deploying the software a test stand with real processing cards is very similar to that of the standalone deployment described above. The difference being that no herd-dummy containers are run on the Shep computer and instead the HERD containers are run on the SoCs of the processing cards.

### Prerequisites
[Docker](https://docs.docker.com/get-docker/) must be installed on both the SoCs running the HERD instances as well as the computer runnig the Shep stack. If you are using a CentOS8 machine, please see the [installation instructions below](#docker-on-centos8).

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the Shep stack from the YAML file, but is not required for the SoCs.

### Running HERD on the ATCA boards

The HERD application (implemented in the `herd-control-app` repository) is an executable that loads SWATCH/HERD plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. There are separate plugin libraries for different flavours of ATCA board, which are packaged up in docker containers using a common GitLab CI pipeline; the following sections describe how to run these containers.

#### Serenity

The Serenity plugin builds on the board-agnostic EMP plugin, and hence registers commands and FSMs both for board-independent EMP configuration procedures and for Serenity-specific procedures. With the [default config file](https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/v0.3.13/herd.yml), the control application will load the Serenity plugin and create one to three devices: one for the Artix (named `artix`), and up to two (named `x0` and `x1`) for the daughter cards which are present on that board.

The control application should typically be run in a CI-built docker container from the Serenity plugin repository; this is the simplest method for using the plugin, as it doesn't require building the code, simply downloading a docker container. Critical info:

  * The image *must* always be run using the `/opt/cactus/bin/serenity/docker-run.sh` script (which wraps `docker run`, adding extra arguments to e.g. make device files accessible inside the container).
  * Image URL: `gitlab-registry.cern.ch/p2-xware/software/serenity-herd/centos7/herd:v0.3.13`

Example command:
   ```shell
   /opt/cactus/bin/serenity/docker-run.sh -d --name herd -p 3000:3000 gitlab-registry.cern.ch/p2-xware/software/serenity-herd/centos7/herd:v0.3.13
   ```

#### Apollo

The [apollo-herd](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd) software provides a plugin library for the [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch) software framework. Specifically, this library registers Apollo-specific controls to the SWATCH framework by wrapping SWATCH code to call functions from the [ApolloSM_plugin](https://github.com/apollo-lhc/ApolloSM_plugin) for [BUTool](https://github.com/BU-Tools/BUTool/tree/develop), whose documentation can be found [here](https://bu-edf.gitlab.io/BUTool/). 

Two classes, [`ApolloDevice`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/ApolloDevice.hpp) and [`ApolloCMFPGA`](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/include/swatch/apolloherd/ApolloCMFPGA.hpp), utilize BUTool and its ApolloSM_plugin to interact with the Service Module (SM) and Command Module (CM) hardware, respectively. In addition, the `ApolloCMFPGA` class builds on top of the [`Processor`](https://gitlab.cern.ch/p2-xware/software/emp-herd/-/blob/master/include/emp/swatch/Processor.hpp) class located in the [emp-herd plugin](https://gitlab.cern.ch/p2-xware/software/emp-herd), which provides the CM control class with functionality for controlling the EMP (Extensible, Modular, Data Processor) firmware framework running on the Command Module's two main FPGAs - the Kintex and Virtex.

The HERD application will read a YAML configuration file, which specifies the devices to instantiate and their properties. If the HERD app is run with [Apollo.yml](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/-/blob/master/Apollo.yml) as the configuration file (the default configuration file), the control application will load the SWATCH plugin and create three devices:

  * One device for the Service Module (named `ApolloSM_device`)
  * Two devices for the Command Module FPGAs (named `kintex` and `virtex`)

At the moment, the `apollo-herd` software can only be launched in a docker container:

  * The image should **always** be run using the `start_apolloherd.sh` shell script. This script wraps the `docker run` command, makes all UIO devices accessible inside the container, mounts several volumes, and sets permissions.
  * While running the container via `start_apolloherd.sh` script, the port `3000` of the container **must** be mapped to the port `3000` of the host Apollo blade, so that the SHEP application can communicate with the container through the Apollo blade (see below). 
  * There are different Docker images based on the architecture on which the container will run (`arm/v7` on the rev.1 blades and `arm64` on the rev.2 blades). However, one can use the multi-arch URL, also created by the GitLab CI, and `docker` command will detect the architecture, and use the relevant image for the arch. The multi-arch URLs can be specified either by a tag name `vX.Y.Z`, or the branch name and the commit hash of the image. The two URL formats are shown below:
    * Tags: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/centos7/herd:vX.Y.Z`
    * Branches: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/centos7/herd:BRANCHNAME-COMMITSHA`  
      where COMMITSHA is the first 8 characters of the git commit's SHA

For the tag-based image URLs, the latest stable tag is `v0.1`, and that image URL will be used for the commands below. 

An example of running the plugin in a container using the non-default config file `/path/to/apollo-config/Apollo.yml` on the host machine, mounted as `/herd.yml` in the container is shown below:

```shell
./path/to/start_apolloherd.sh -d -p 3000:3000 -v /path/to/apollo-config/Apollo.yml:/herd.yml gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/centos7/herd:v0.1
```

The above command:

1. Runs the container in the background with the -d flag
2. Maps TCP port 3000 in the container to 3000 on the Docker host. This is necessary for the SHEP application to communicate with the Apollo blade on port `3000`.
3. Mounts the configuration file `/path/to/apollo-config/Apollo.yml` on the host to `/herd.yml` in the container. Note that the desination file path **must** be specified as `/herd.yml` as the HERD plugin is looking for this config file as an input, in the root directory of the container.   
4. Specifies the appropriate apollo-herd image to run (with the specified image URL), and,
5. Starts the HERD control app with the appropriate Apollo-specific config file, `/herd.yml`

Note that the last argument, the Docker image URL to run, must be correctly specified. To get a list of available images and their URLs, one can consult the Container Registry section for `centos7/herd` images in GitLab, which can be found [here](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/container_registry/12769). Once the image is installed locally, it is also possible to give the image ID as an argument, instead of the image URL. To check which images are downloaded locally, you can execute `docker images`.

To run the plugin in a container with the default config file, the `-v` option could be ommitted:

```shell
./path/to/start_apolloherd.sh -d -p 3000:3000 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/centos7/herd:v0.1
```

Similar to the command above, this commmand will launch the HERD plugin with the default configuration file already contained within the specified image.

For debugging the image, it is possible to run `start_apolloherd.sh` with the flag combination `-it --entrypoint bin/bash`, in order to run the container interactively without the HERD control application starting up. Once inside the container, the HERD application can be started manually. You can use the `start_apolloherd.sh` script as follows to run the container interactively:

```shell
# Run the container interactively, without starting the HERD application
./path/to/start_apolloherd.sh -p 3000:3000 -it --entrypoint bin/bash gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/apollo-herd/centos7/herd:v0.1
```

And once you're inside the container, you can set up the environment by sourcing the `entrypoint_env.sh` script, and start the HERD application by using the `herd.yml` configuration file inside the container, as follows:

```shell
# Once inside the container, set up the environment and launch the HERD application
source entrypoint_env.sh
herd herd.yml
```

### Running Shep
The docker-compose YAML file for the shep stack can be found under [deployments/docker-compose](deployments/docker-compose). This repo should either be cloned or that directory copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Start the containers (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer).

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md)


## Remote control from the command line

You can also remotely control a HERD application from the command line by running the console script. In particular, the command-line interface can be useful when developing plugins, as it allows you to quickly run commands without having to start up the full set of containers for the shep layer. This console and its dependencies are packaged in a docker container, so you run it as follows (replacing `BOARD_HOSTNAME` with your board's IP address or hostname):
```
docker run -it --network=host gitlab-registry.cern.ch/cms-cactus/phase2/pyswatch/centos7/console:v0.3.0 BOARD_HOSTNAME
```

Specifically, in this console you can:
 * list device, command and FSM information by typing `info`;
 * run commands and FSM transitions by typing `run DEVICE_ID COMMAND_OR_TRANSITION_ID` (e.g. `run x0 reset`);
 * engage FSMs by typing `engage-fsm DEVICE_ID FSM_ID` (e.g. `engage-fsm x0 myFSM`); 
 * reset FSMs by typing `reset-fsm DEVICE_ID` (e.g. `reset-fsm x0`); and
 * disengage FSMs by typing `disengage-fsm DEVICE_ID` (e.g. `disengage-fsm x0`).


## Setting up container runtimes

### Installing docker on CentOS8

On RedHat 7, docker can be used out-of-the box by simply following the standard docker install instructions. On RedHat 8 - and derivatives like CentOS 8 - the default container runtime engine is Podman, and so in order for docker to work, you need to first run a dedicated command to install `containerd`, and also add a firewall masquerade. The following commands have been verified to work on a few computers:
```
# Install docker
sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo sed -i 's|\$releasever|7|g' /etc/yum.repos.d/docker-ce.repo 
sudo dnf install -y docker-ce-19.03.11 docker-ce-cli-19.03.11
sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo sed -i 's|\$releasever|7|g' /etc/yum.repos.d/docker-ce.repo 
sudo dnf install -y docker-ce-19.03.11 docker-ce-cli-19.03.11

# Add firewall masquerade, then restart docker
sudo firewall-cmd --zone=public --add-masquerade --permanent
sudo firewall-cmd --reload
sudo systemctl restart docker
sudo systemctl is-active docker

# Verify that DNS working in containers
docker run centos:8 getent hosts google.com
```

### Podman: Post-install setup instructions

When you install podman on CentOS8 (or other RHEL8 derivatives), it will download containers
under `/var/lib/containers` by default, but only `root` has access to that directory. As a result
you will run into permission-related errors if running containers with your normal user account.
One solution is to instead configure podman to store them in a different directory. When creating
that directory you need to open up write permissions to at least your user account, and also update
the SELinux settings for that directory - e.g. for `/data/containers`:

```
sudo mkdir /data/containers
sudo chmod -R a+rw /data/containers
sudo semanage fcontext -a -e /var/lib/containers /data/containers
sudo restorecon -R -vv /data/containers
```

You can then configure podman to use this new directory by setting `graphroot` in
`/etc/containers/storage.conf` - i.e:

```
# Primary Read/Write location of container storage
#graphroot = "/var/lib/containers/storage"
graphroot = "/data/containers"
```

### Podman: Running SHEP with Podman Backend

If the containers are going to be run with a Podman backend, `podman-compose` utility can also be used to run the SHEP stack. For this purpose, a tested `podman-compose.yml` file can be found under the directory `deployments/docker-compose`. This new `.yml` file introduces minor changes to the corresponding `docker-compose.yml` file to make the configuration compatible with Podman. The main changes are updating the logging driver to `k8s-file` and introducing volume labeling for the mounted volumes (`:Z`) so that the Podman containers have proper permissions to open the files contained in the volumes.

To run the SHEP stack with Podman, make sure that the `podman-plugins` package is installed, which contains a DNS name resolver that will be used by the containers. If that is not installed, you can install it as follows:

```
sudo dnf install -y podman-plugins
```

Next, in order to run the SHEP stack in rootless mode (i.e. without `sudo`), a user-level socket must be created. This can be done as follows:

```
# Enable and start the user-level Podman socket
systemctl --user enable podman.socket
systemctl --user start podman.socket

# Check that the user-level socket is running
systemctl --user status podman.socket

# Enable lingering so that the container keeps running after you've logged out
loginctl enable-linger $(whoami)
```
Note: The above commands only need to be run once per user after system installation - the `enable` command ensures that the user-level socket will then be recreated whenever the computer is rebooted in the future.

Once this is done, you can execute the SHEP containers as follows:

```
# Start the containers
cd deployments/docker-compose
podman-compose -f podman-compose.yml up -d

# Check the running containers
podman-compose -f podman-compose.yml ps

# Stop and remove the containers
podman-compose -f podman-compose.yml down
```

## Project links

| Project |
|---------|
| [herd-control-app](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/) |
| [herd-dummy](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/) |
| [shep-api-server](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/) |
| [shep-ui](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep/) |
