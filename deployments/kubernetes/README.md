# Kubernetes
For large scale deployments, the recommended method is a Kubernetes cluster. The cluster should be setup such that all processing cards are nodes within the cluster as well as sufficient server nodes to host the shep level pods.

This section is still under active development and should **not** be used by non-developers.

# Environment
To run this setup a running kubernetes cluster is required. For local development [K3D](https://k3d.io/) is recommended and assumed for the rest of this guide. [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/) can also be used to running local Kubernetes.


## Helm Setup
[Helm](https://helm.sh) is required for installing Loki.
```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

## Cluster Setup using K3D
The following commands create a k3d cluster with a master and worker node.
```bash
k3d cluster create demo -a 2
```

The following command updates kubectl to direct future commands to this new cluster.
```bash
kubectl config use-context k3d-demo
```

To test the daemonset aproach, one of the nodes must be labeled and tainted so that other pods don't execute on that node.
```bash
kubectl label nodes k3d-demo-agent-1 role=device
kubectl taint nodes k3d-demo-agent-1 role=device:NoSchedule
```

## Cluster Setup using Kind
To customize your cluster, you can provide additional configuration. The following is a sample kind configuration `kind-config.yaml`. 
```bash
kind: Cluster
apiVersion: kind.sigs.k8s.io/v1alpha3
nodes:
  - role: control-plane
  - role: worker
```

The following commands create a kind cluster with a master and worker node.
```bash
kind create cluster --config kind-config.yaml
```

The next steps are similar to K3D:
The following command updates kubectl to direct future commands to this new cluster.
```bash
kubectl config use-context k3d-demo
```

To test the daemonset aproach, one of the nodes must be labeled and tainted so that other pods don't execute on that node.
```bash
kubectl label nodes k3d-demo-agent-1 role=device
kubectl taint nodes k3d-demo-agent-1 role=device:NoSchedule
```

# Storage
For the local deployment via K3D, this section is not relevant as it already provides a default storage option. However, if you are deploying this on a production system a number of components within benefit from having a distributed persistent storage option available.  
To facilitate this, [Longhorn](https://longhorn.io/) configurations are included in this repository.

To install Longhorn the following commands are required:
```bash
helm repo add longhorn https://charts.longhorn.io
helm repo update
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --values storage/longhorn/values.yaml
```

Once Longhorn has been installed, two configuration files have to be updated to tell the persistant storage requests to use it. These are `loki/values.yaml` and `shep/api/database/deployment.yaml` and they both have the line `storageClassName: local-path` which needs to be changed to `storageClassName: longhorn`.

# Deployment
Running the following command from the location of `kustomization.yaml` will install the entire system.

```bash
kubectl apply -k .
```

The following command will wait until the system is up and running.
```bash
kubectl wait --for=condition=Ready pod -l "app in (shep-database,shep-api,shep-ui,shep-heartbeat,herd-dummy)"
```

## Monitoring
```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

helm upgrade --install loki grafana/loki -f loki/values.yaml
kubectl apply -f loki/datasource.yaml

helm upgrade --install promtail grafana/promtail -f promtail/values.yaml

helm upgrade --install cms grafana/grafana -f grafana/values.yaml
```

### Grafana
To extract the password.
```bash
kubectl get secret --namespace default cms-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

To port forward from kubernetes to localhost.
```bash
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=cms" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace default port-forward $POD_NAME 3000
```

To access it you need to go to http://localhost:3000 and log in with the user `admin` and the generated password.

### Troubleshooting: reset Grafana passwords
Sometimes users often forget the admin password after the first login. There are actually two methods to reset Grafana password

#### Option 1: grafana-cli

"Grafana-cli" is often used to reset the admin password. 
```bash
grafana-cli admin reset-admin-password
```

However, this command needs to know where is your configuration file and Grafana home directory, because from the configuration it will extract what kind of back-end server you use like sqlite3 or MySQL (or other) and the path or login data if needed to the back-end, then it will reset the password with the given one.

So first you should check with what command-line arguments was your instance of Grafana server started:
```bash
[root@monitoring ~]# ps axuf|grep grafana
grafana    490  0.0  0.0 1274696 30344 ?       Ssl  14:18   0:09 /usr/sbin/grafana-server --config=/etc/grafana/grafana.ini --pidfile=/var/run/grafana/grafana-server.pid cfg:default.paths.logs=/var/log/grafana cfg:default.paths.data=/var/lib/grafana cfg:default.paths.plugins=/var/lib/grafana/plugins cfg:default.paths.provisioning=/etc/grafana/provisioning
```
This case has the config in “/etc/grafana/grafana.ini”, plugins and sessions are in “/var/lib/grafana”, logs in “/var/log/” and the home directory as of grafana-cli expect to be is in “/usr/share/grafana” (look below to see why you cannot use this directory for proper resetting of the admin password). So you should pass 2 additional arguments to be sure “grafana-cli admin” will reset the admin password in the right place:

For MacOS, the locations for the files are listed before:
##### Configuration

The Configuration file should be located at `/usr/local/etc/grafana/grafana.ini`.

##### Logs

The log file should be located at `/usr/local/var/log/grafana/grafana.log`.

##### Plugins

If you want to manually install a plugin place it here: `/usr/local/var/lib/grafana/plugins`.

##### Database

The default sqlite database is located at `/usr/local/var/lib/grafana`

#### Option 2: without grafana-cli
First you should find your Grafana configuration file, under MacOS it is `/usr/local/etc/grafana/grafana.ini`

Open the file and look for lines like:
```bash
#################################### Database ############################
[database]
# You can configure the database connection by specifying type, host, name, user and password
# as seperate properties or as on string using the url propertie.
 
# Either "mysql", "postgres" or "sqlite3", it's your choice
type = sqlite3
host = 127.0.0.1:3306
name = grafana
user = root
```

As you can see this configuration says the type of the database is “sqlite3” and the file of the database is in /var/lib/grafana – /var/lib/grafana/grafana.db. So execute the following to reset the password:

```bash
[root@monitorin ~]# sqlite3 /var/lib/grafana/grafana.db
SQLite version 3.7.17 2013-05-20 00:56:22
Enter ".help" for instructions
Enter SQL statements terminated with a ";"
sqlite> .tables
alert                   dashboard_version       quota                
alert_notification      data_source             session               
annotation              login_attempt           star                  
annotation_tag          migration_log           tag                   
api_key                 org                     team                  
dashboard               org_user                team_member           
dashboard_acl           playlist                temp_user             
dashboard_provisioning  playlist_item           test_data             
dashboard_snapshot      plugin_setting          user                  
dashboard_tag           preferences             user_auth             
sqlite> select * from user;
1|0|admin|admin@example.com|Admin|34523452daaaeeeefffw558687994560498b1c6e9a78253413363334e453459a8afb1f08b43aaa6feb3d12cb0e417b81e209|Z6ffE7yyzc|46pd5MzKYv||1|1|0|dark|2016-06-01 22:44:24|2018-09-20 08:05:51|1|2018-09-23 15:02:11
3|0|myuser@example.com|myuser@example.com|My User|099776920940da2aaafeefae1f5234505288c72345723452350fffefefedadafafdcc8338fe8498ffe665d9489ac406fdbc6|TCPPLiodsx|7czPOlNmr5||1|0|0||2018-09-21 13:07:43|2018-09-21 13:07:43|0|2018-09-21 13:19:28
sqlite> update user set password = '59acf18b94d7eb0694c61e60ce44c110c7a683ac6a8f09580d626f90f4a242000746579358d77dd9e570e83fa24faa88a8a6', salt = 'F3FAxVm33R' where login = 'admin';
sqlite> 
```
Now the grafana admin password has been reset into "admin"

# Interacting with the SHEP UI
The following command will return the IP address used to access the SHEP UI via a web browser.
```bash
echo `kubectl get -n kube-system svc/traefik -o jsonpath="{.status.loadBalancer.ingress[0].ip}"
```
